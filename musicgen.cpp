
// generate songs.txt and music.asset for eu4

#include <iostream>
#include <fstream>
#include <string>
#include <vector>


std::string noext(char* filename)
{
	std::string s(filename);
	
	return s.substr(0, s.length() - 4);
}

void getExistingSongs(std::vector<std::string> &list)
{
	//std::cout << "Searching for existing songs...\n";
	
	std::ifstream songs_file("mod_songs.txt", std::ios::in);
	
	std::string line;
	
	while(songs_file.good())
	{
		std::getline(songs_file, line);
		
		size_t pos = line.find("name");
		
		if (pos != std::string::npos)
		{
			pos = line.find("\"");
			
			if (pos != std::string::npos)
			{
			
				line = line.substr(pos+1);
				pos = line.find("\"");
				
				if (pos != std::string::npos)
				{
					line = line.substr(0, pos);
										
					list.push_back(line);
					std::cout << "      " << line << std::endl;
				}
			}
		}
	}
	
	songs_file.close();
}

bool isDuplicate(std::vector<std::string> &list, std::string str)
{
	bool dupe = false;
	
	for (int i = 0; i < list.size(); i++)
	{
		if (list.at(i) == str)
		{
			dupe = true;
			break;
		}
	}
	
	return dupe;
}

int main (int argc, char** argv)
{
	std::vector<std::string> song_list;
	
	if (argc < 2)
	{
		std::cout << "Invalid number of args.\n";
	}
	else
	{
		getExistingSongs(song_list);
		
		std::ofstream songs("mod_songs.txt", std::ios::app);
		std::ofstream asset("mod_music.asset", std::ios::app);
		
		if (songs.is_open() && asset.is_open())
		{
			
			//std::cout << "Writing new songs to songs.txt and music.asset...\n";
			for (int i = 1; i < argc; i++)
			{
				if (!isDuplicate(song_list, noext(argv[i])))
				{
					std::string s = noext(argv[i]);
					// append to songs.txt
					songs << "song = {\n\tname = \"" << s
						<< "\"\n\n\tchance = {\n\t\tmodifier = {\n\t\t\tfactor = 1\n\t\t}\n\n\t\t## OPTIONAL MODIFIERS ##\n\t}\n\n}\n\n";
					
					// append to music.asset
					asset << "music = {\n\tname = \"" << s << "\"\n\tfile = \"" << argv[i] << "\"\n}\n\n";
					
					std::cout << " NEW  " << s << std::endl;
				}
				
			}
		}
		
		songs.close();
		asset.close();
	}
	
	return 0;
}