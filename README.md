# EU4 songs.txt and music.asset generator

Run from command line via `mgen file1 file2 ...` or `mgen *.ogg` to automatically generate the files `songs.txt` and `music.asset` needed to add music to Europa Universalis IV. 